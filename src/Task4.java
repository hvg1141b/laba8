/* 4. Вывести на экран список ключей Есть коллекция HashMap<String, String>, туда
 * занесли 10 различных строк. Вывести на экран список ключей, каждый элемент с новой строки.
 * 5. Вывести на экран список значений Есть коллекция HashMap<String, String>, туда
 * занесли 10 различных строк. Вывести на экран список значений, каждый элемент с новой строки.
 */

/**
 *
 * @author Виктор
 */

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Task4 {
    public static void main(String[] args) throws Exception
    {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("qwe", "qwe");
        map.put("rty", "rty");
        map.put("uio", "uio");
        map.put("pas", "pas");
        map.put("dfg", "dfg");
        map.put("hjk", "hjk");
        map.put("lzx", "lzx");
        map.put("cvb", "cvb");
        map.put("nmq", "nmq");
        map.put("wer", "wer");
        //4. Выводим ключи
        printKeys(map);
        //5. Выводим значения
        printValues(map);
    }

    public static void printKeys(Map<String, String> map)
    {
        for (Map.Entry<String, String> pair : map.entrySet()){
            System.out.println(pair.getKey());
        }
}
    
    public static void printValues(Map<String, String> map)
    {
        for (Map.Entry<String, String> pair : map.entrySet()){
            System.out.println(pair.getValue());
        }
    }
}
