/* 6. Коллекция HashMap из Object Есть коллекция HashMap<String, Object>, туда занесли
 * 10 различных пар объектов. Вывести содержимое коллекции на экран, каждый элемент
 * с новой строки.Пример вывода (тут показана только одна строка): Sim - 5
 */

/**
 *
 * @author Виктор
 */

import java.util.HashMap;
import java.util.Map;

public class Task6 {
    public static void main(String[] args) throws Exception
    {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("Sim", 5);
        map.put("Sim2", 6);
        map.put("Sim3", 7);
        map.put("Sim4", 8);
        map.put("Sim5", "Sim");
        map.put("Sim6", 9);
        map.put("Sim7", 10);
        map.put("Sim8", 11);
        map.put("Sim9", 12);

        for (HashMap.Entry<String,Object>pair:map.entrySet()){
            String key = pair.getKey();
            Object value = pair.getValue();
            System.out.println(key+ " - " + value );
        }
    }
}
