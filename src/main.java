/* 1. HashSet из растений Создать коллекцию HashSet с типом элементов String. Добавить
 * в неё 10 строк: арбуз, банан, вишня, груша, дыня, ежевика, жень-шень, земляника,
 * ирис, картофель. Вывести содержимое коллекции на экран, каждый элемент с новой
 * строки.Посмотреть, как изменился порядок добавленных элементов.
 * 2. HashMap из 10 пар Создать коллекцию HashMap<String, String>, занести туда 10
 * пар строк: арбуз - ягода, банан - трава, вишня - ягода, груша - фрукт, дыня - овощ,
 * ежевика - куст, жень-шень - корень, земляника - ягода, ирис - цветок, картофель - клубень.
 * Вывести содержимое коллекции на экран, каждый элемент с новой строки. Пример вывода
 * (тут показана только одна строка):картофель - клубень
 */

/**
 *
 * @author Виктор
 */

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class main {
    public static void main(String[] args) {    
    //1. HashSet из растений
        Set<String> set = new HashSet<>();
        set.add("арбуз");
        set.add("банан");
        set.add("вишня");
        set.add("груша");
        set.add("дыня");
        set.add("ежевика");
        set.add("жень-шень");
        set.add("земляника");
        set.add("ирис");
        set.add("картофель");
        for (String text : set)
        {
            System.out.println(text);
        }
    
    //2. HashMap из 10 пар
    Map<String, String> map = new HashMap<>();
        map.put("арбуз", "ягода");
        map.put("банан", "трава");
        map.put("вишня", "ягода");
        map.put("груша", "фрукт");
        map.put("дыня", "овощ");
        map.put("ежевика", "куст");
        map.put("жень-шень", "корень");
        map.put("земляника", "ягода");
        map.put("ирис", "цветок");
        map.put("картофель", "клубень");    
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, String>pair = iterator.next();
            String key = pair.getKey();
            String value = pair.getValue();
            System.out.println(key + " – " + value);
        }
}
 }